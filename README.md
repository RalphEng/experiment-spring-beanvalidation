Experiment: Spring and Beanvalidation (JSR 303)
===============================================

Question 1: how can a (custom) ConstraintValidator have Spring Beans?
---------------------------------------------------------------------

Quite simple: the Constraint Validator can have a constructor that take the Spring Beans.
The Spring ValidatorFactory will pass the Beans to the constructor when the Constraint is initialized.
There is no `@Autowired` annotation needed!

```
public class BeanAccessingMinValueValidator implements ConstraintValidator<BeanAccessingMinValue, Integer>{

    private DemoService demoService;
    
    public BeanAccessingMinValueValidator(DemoService demoService) {
        this.demoService = demoService;
    }
    
    public void initialize(BeanAccessingMinValue constraint) {}

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
       ...
    }

}
```

Question 2: how pass extra context information to a (custom) ConstraintValidator
--------------------------------------------------------------------------------

> From time to time, you might want to condition the constraint validator behavior on some external parameters.
> ...
> The notion of constraint validator payload was introduced for all these use cases.
> It is an object passed from the Validator instance to each constraint validator via the `HibernateConstraintValidatorContext`.

[6.1.2.3. Passing a payload to the constraint validator](https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#constraint-validator-payload)

Important: the mentioned "constraint validator payload" is *not* the stuff from the constraint annotation: `Class<? extends Payload>[] payload() default {};`
and it is also not what Hibernate-Validation call the "dynamic payload" that is incuded in the *raided* violations!

Context information can be added to the `*Hibernate*ValidatorContext` via: `constraintValidatorPayload`.
The context information can be accessed in the contraint validator from `*Hibernate*ConstraintValidatorContext`.
It is identified by its type!

Adding context information:
```
@Autowired private ValidatorFactory validatorFactory;
...
HibernateValidatorFactory hibernateValidatorFactory = validatorFactory.unwrap(HibernateValidatorFactory.class);
HibernateValidatorContext validatorContext = hibernateValidatorFactory
                .usingContext()
                .constraintValidatorPayload(lowerBoundProvider);
Validator validator = validatorContext.getValidator();

Set<ConstraintViolation<...>> violations = validator.validate(demoEntity);
```

Access the context information
```
public class ContextReleatedMinValueValidator implements ConstraintValidator<ContextReleatedMinValue, Integer>{

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {        
        LowerBoundProvider lowerBoundProvider = 
        	context.unwrap(HibernateConstraintValidatorContext.class)
        	.getConstraintValidatorPayload(LowerBoundProvider.class);
        
        return value >= lowerBoundProvider.lowerBound();
    }
}

```
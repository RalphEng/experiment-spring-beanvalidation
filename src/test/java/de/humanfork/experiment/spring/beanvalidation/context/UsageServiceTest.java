package de.humanfork.experiment.spring.beanvalidation.context;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import de.humanfork.experiment.spring.beanvalidation.DemoEntity;

@SpringBootTest
public class UsageServiceTest {

    @Autowired
    private UsageService usageService;

    @Test
    public void testValidate() {

        DemoEntity demoEntity = new DemoEntity(5);
        assertThat(this.usageService.validate(demoEntity, () -> 3)).isEmpty();

        Set<ConstraintViolation<DemoEntity>> violations = this.usageService.validate(demoEntity,
                () -> 6);
        assertThat(violations).hasSize(1);
        ConstraintViolation<DemoEntity> violation = violations.iterator().next();
        assertThat(violation.getPropertyPath().toString()).isEqualTo("checkValue"); //do not use Paht.toString in production
    }

}

package de.humanfork.experiment.spring.beanvalidation;

import static org.assertj.core.api.Assertions.assertThat;

import javax.validation.Validator;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import de.humanfork.experiment.spring.beanvalidation.beanaccess.DemoService;

@SpringBootTest
class DemoApplicationTests {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private Validator validator;

    @Autowired
    private DemoService demoService;

    @Test
    void contextLoads() {
        assertThat(this.applicationContext).isNotNull();
    }

    @Test
    public void testValidator() {
        assertThat(this.validator).isNotNull();
    }

    @Test
    void demoServiceBean() {
        assertThat(this.demoService).isNotNull();
    }

}

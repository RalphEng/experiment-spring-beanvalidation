package de.humanfork.experiment.spring.beanvalidation.beanaccess;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import de.humanfork.experiment.spring.beanvalidation.DemoEntity;

@SpringBootTest
public class BeanAccessingMinValueValidatorTest {

    @Autowired
    private Validator validator;

    @Autowired
    private DemoService demoService;

    @Test
    public void testValidatorCanAccessBean() {
        DemoEntity validEntity = new DemoEntity(this.demoService.doServiceStuff() + 1);
        DemoEntity invalidEntity = new DemoEntity(this.demoService.doServiceStuff() - 1);
        
      //ContextReleatedMinValue.class is also used as validation group, see DemoEntity.checkValue
        assertThat(this.validator.validate(validEntity, BeanAccessingMinValue.class)).isEmpty();

        Set<ConstraintViolation<DemoEntity>> violations = this.validator.validate(invalidEntity, BeanAccessingMinValue.class);
        assertThat(violations).hasSize(1);
        ConstraintViolation<DemoEntity> constraintViolation = violations.iterator().next();
        assertThat(constraintViolation.getPropertyPath().toString()).isEqualTo("checkValue"); //do not use Paht.toString in production
    }

}

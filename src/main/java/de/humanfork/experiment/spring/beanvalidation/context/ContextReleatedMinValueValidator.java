package de.humanfork.experiment.spring.beanvalidation.context;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;

public class ContextReleatedMinValueValidator implements ConstraintValidator<ContextReleatedMinValue, Integer> {

    @Override
    public boolean isValid(final Integer value, final ConstraintValidatorContext context) {

        LowerBoundProvider lowerBoundProvider = context.unwrap(HibernateConstraintValidatorContext.class)
                .getConstraintValidatorPayload(LowerBoundProvider.class);
        if (lowerBoundProvider == null) {
            throw new IllegalStateException("no LowerBoundProvider found");
        }

        return value >= lowerBoundProvider.lowerBound();
    }
}

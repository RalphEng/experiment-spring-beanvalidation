package de.humanfork.experiment.spring.beanvalidation.context;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ContextReleatedMinValueValidator.class)
public @interface ContextReleatedMinValue {

    String message() default "{de.humanfork.experiment.spring.beanvalidation.context.BeanAccessingMinValueValidator.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package de.humanfork.experiment.spring.beanvalidation.beanaccess;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * A Bean Validation 2.0-Validator, that access a spring bean.
 *
 * <p>
 * Note that there is no {@literal@}Autowired} annotation, instead the needed Bean is just a simple constructor
 * parameter!
 * </p>
 *
 */
public class BeanAccessingMinValueValidator implements ConstraintValidator<BeanAccessingMinValue, Integer> {

    private DemoService demoService;

    public BeanAccessingMinValueValidator(final DemoService demoService) {
        this.demoService = demoService;
    }

    @Override
    public void initialize(final BeanAccessingMinValue constraint) {
    }

    @Override
    public boolean isValid(final Integer value, final ConstraintValidatorContext context) {
        return value >= this.demoService.doServiceStuff();
    }

}

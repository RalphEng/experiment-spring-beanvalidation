package de.humanfork.experiment.spring.beanvalidation.context;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.hibernate.validator.HibernateValidatorContext;
import org.hibernate.validator.HibernateValidatorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.humanfork.experiment.spring.beanvalidation.DemoEntity;

@Service
public class UsageService {

    @Autowired
    private ValidatorFactory validatorFactory;

    public Set<ConstraintViolation<DemoEntity>> validate(final DemoEntity demoEntity,
            final LowerBoundProvider lowerBoundProvider) {

        HibernateValidatorFactory hibernateValidatorFactory = this.validatorFactory
                .unwrap(HibernateValidatorFactory.class);
        HibernateValidatorContext validatorContext = hibernateValidatorFactory.usingContext()
                .constraintValidatorPayload(lowerBoundProvider);

        Validator validator = validatorContext.getValidator();
        //ContextReleatedMinValue.class is also used as validation group, see DemoEntity.checkValue
        return validator.validate(demoEntity, ContextReleatedMinValue.class);
    }
}

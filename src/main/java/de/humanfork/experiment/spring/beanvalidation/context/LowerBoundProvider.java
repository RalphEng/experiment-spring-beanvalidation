package de.humanfork.experiment.spring.beanvalidation.context;

@FunctionalInterface
public interface LowerBoundProvider {

    public int lowerBound();

}

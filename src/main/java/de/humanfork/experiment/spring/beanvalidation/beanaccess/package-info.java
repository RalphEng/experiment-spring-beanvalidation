/**
 * Experiment: how can a (custom) ConstraintValidator have Spring Beans?
 * 
 * The Constraint Validator can have a constructor that take the Spring Beans.
 * The Spring ValidatorFactory will pass the Beans to the constructor when the Constraint is initialized.
 * There is no @Autowired annotation needed!
 */
package de.humanfork.experiment.spring.beanvalidation.beanaccess;

package de.humanfork.experiment.spring.beanvalidation.beanaccess;

import org.springframework.stereotype.Service;

/**
 * This Bean is used by the constaint validator {@link BeanAccessingMinValueValidator}.
 */
@Service
public class DemoService {

    public int doServiceStuff() {
        return 10;
    }

}

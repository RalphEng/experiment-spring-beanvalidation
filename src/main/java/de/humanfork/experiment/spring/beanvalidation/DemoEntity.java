package de.humanfork.experiment.spring.beanvalidation;

import de.humanfork.experiment.spring.beanvalidation.beanaccess.BeanAccessingMinValue;
import de.humanfork.experiment.spring.beanvalidation.context.ContextReleatedMinValue;

public class DemoEntity {

    /**
     * About the contraints:
     * There are two constraints in two different validation groups to test independent of each other.
     * The identifying class of each group is the validation annotation class itself.
     */
    @BeanAccessingMinValue(groups = BeanAccessingMinValue.class)
    @ContextReleatedMinValue(groups = ContextReleatedMinValue.class)
    private int checkValue;

    public DemoEntity(final int checkValue) {
        this.checkValue = checkValue;
    }

    public int getCheckValue() {
        return this.checkValue;
    }

}

/**
 * Experiment: how pass extra context information to a (custom) ConstraintValidator
 * 
 * {@link de.humanfork.experiment.spring.beanvalidation.context.UsageService}:
 * <pre><code>
 * HibernateValidatorFactory hibernateValidatorFactory = validatorFactory.unwrap(HibernateValidatorFactory.class);
 * HibernateValidatorContext validatorContext = hibernateValidatorFactory
 *                 .usingContext()
 *                 .constraintValidatorPayload(lowerBoundProvider);
 * Validator validator = validatorContext.getValidator();
 * 
 * Set<ConstraintViolation<...>> violations = validator.validate(demoEntity); 
 * </code></pre>
 * 
 * {@link de.humanfork.experiment.spring.beanvalidation.context.ContextReleatedMinValueValidator}:
 * <pre><code>
 * public class ContextReleatedMinValueValidator implements ConstraintValidator<ContextReleatedMinValue, Integer>{
 * 
 *     @Override
 *     public boolean isValid(Integer value, ConstraintValidatorContext context) {        
 *         LowerBoundProvider lowerBoundProvider = 
 *             context.unwrap(HibernateConstraintValidatorContext.class)
 *             .getConstraintValidatorPayload(LowerBoundProvider.class);
 *         
 *         return value >= lowerBoundProvider.lowerBound();
 *     }
 * } 
 * </code></pre>
 * 
 * 
 * @see https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#constraint-validator-payload
 */
package de.humanfork.experiment.spring.beanvalidation.context;
